define(['config'], function (config) {
    function Card(fileName1) {
        PIXI.Container.call(this);
        this.create(fileName1);
    }

    Card.prototype = Object.create(PIXI.Container.prototype);

    Card.prototype.create = function (fileName1) {
        var card = PIXI.Sprite.fromImage(fileName1);
        card.scale.set(0.25);
        card.anchor.set(0, 1);
        config.stage.addChild(this);
        return this.addChild(card);
    }

    return Card;
})