define(['Player', 'Card', 'config'], function (Player, Card, config) {

    function Dealer() {
        this.cardPosition = {x: 540, y: 200};
        this.cardXMovement = 25;
        Player.apply(this, arguments);
    }

    Dealer.prototype = Object.create(Player.prototype);

    Dealer.prototype.drawCards = function () {
        var card;
        var randNumber = this.getRandomNumber(this.cardsNumbers.length);
        card = this.createCard('' + this.cardsNumbers[randNumber] + '_of_' + this.cardsColors[this.getRandomNumber(this.cardsColors.length)] + '.png');
        card.x = this.cardPosition.x;
        card.y = this.cardPosition.y;
        card.points = config.points[randNumber];
        this.cards.push(card);

        var randNumber = this.getRandomNumber(this.cardsNumbers.length);
        card = this.createCard('' + this.cardsNumbers[randNumber] + '_of_' + this.cardsColors[this.getRandomNumber(this.cardsColors.length)] + '.png');
        card.x = this.cardPosition.x + this.cardXMovement;
        card.y = this.cardPosition.y;
        card.points = config.points[randNumber];
        this.cards.push(card);
    }

    Dealer.prototype.getRandomNumber = function (from) {
        this.randomNumber = Math.floor(Math.random() * from);
        return this.randomNumber;
    }

    return Dealer;
})