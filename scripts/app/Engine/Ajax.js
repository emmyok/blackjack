define(function() {
    var createCORSRequest = function(method, url) {
        var xhr = new XMLHttpRequest();
        if ("withCredentials" in xhr) {
            xhr.open(method, url, true);
        } else if (typeof XDomainRequest != "undefined") {
            xhr = new XDomainRequest();
            xhr.open(method, url);
        } else {
            xhr = null;
        }
        return xhr;
    }

    function Ajax() {}

    Ajax.prototype.call = function(url, callback){
        var xhr = createCORSRequest('GET', url);
        if (!xhr) {
            console.log('CORS not supported');
            return;
        }
        xhr.onload = function() {
            if (xhr.readyState == 4 && xhr.status == 200) {
                if (callback){
                    callback(xhr.responseText);
                }
            }
        };
        xhr.onerror = function() {
            console.log('There was an error making the request.');
        };
        xhr.send();
    }

    return Ajax;
})