define(function() {
    return {
        EVENTS: {
            ASSETS_LOADED: "assets.loaded",
            ASSETS_FILE_LOADED: "assets.file.loaded"
        }
    }
})