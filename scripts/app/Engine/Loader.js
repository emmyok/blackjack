define(['Engine/Events', 'Engine/Enum', 'Engine/Ajax'], function (events, eNum, Ajax) {
    function Loader() {}

    Loader.prototype.setAssetsFromArray = function (assets) {
        this.assets = assets;
    }

    Loader.prototype.setAssetsFromFile = function (fileName) {
        var ajax = new Ajax();
        ajax.call(fileName, this.fileLoaded.bind(this))
    }

    Loader.prototype.fileLoaded = function (assets) {
        this.assets = JSON.parse(assets);
        events.emit(eNum.EVENTS.ASSETS_FILE_LOADED);
    }

    Loader.prototype.load = function () {
        PIXI.loader
            .add(this.assets)
            .load(this.onComplete);
    }

    Loader.prototype.onComplete = function () {
        events.emit(eNum.EVENTS.ASSETS_LOADED);
    }

    return Loader;
})