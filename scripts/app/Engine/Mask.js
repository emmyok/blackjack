define(['pixi'],function(pixi) {
    var GRAPHICS = PIXI.Graphics;

    function Mask(whatToMask, x, y, width, height) {
        if (whatToMask) this.whatToMask = whatToMask;
        if (x) this.x = x;
        if (y) this.y = y;
        if (width) this.width = width;
        if (height) this.height = height;
    }

    Mask.prototype.setWidth = function(width) {
        this.width = width;
    }

    Mask.prototype.setHeight = function(height) {
        this.height = height;
    }

    Mask.prototype.setWhatToMask = function(whatToMask) {
        this.whatToMask = whatToMask;
    }

    Mask.prototype.setPosition = function(x, y) {
        this.x = x;
        this.y = y;
    }

    Mask.prototype.create = function() {
        if (!this.x || !this.y || !this.width || !this.height) return;

        var mask = new GRAPHICS();
        pixi.getStage().addChild(mask);
        mask.beginFill();
        mask.drawRect(0, 0, this.width, this.height);
        mask.position.x = this.x;
        mask.position.y = this.y;
        mask.endFill();
        if (this.whatToMask) this.whatToMask.mask = mask;
        return mask;
    }

    return Mask;
})