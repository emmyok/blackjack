define(['config'], function (config) {

    function HoldButton() {
        this.createButtonHold();
        this.createButtonresult();
    }


    HoldButton.prototype.createButtonHold = function () {
        var buttons = new PIXI.Container();
        buttons.addChild(this.createButtonAddCardDealer());
        config.stage.addChild(buttons);
        return buttons;
    }

    HoldButton.prototype.createButtonresult= function () {
        console.log(9999)
        var buttonResult = new PIXI.Container();
        buttonResult.addChild(this.createButtonResults());
        config.stage.addChild(buttonResult);
        return buttonResult;
    }

    HoldButton.prototype.createButtonAddCardDealer = function () {
        var buttonText = new PIXI.Text('Hold Player',
            {
                fontFamily: 'Arial',
                fontSize: 24,
                fill: 'white',
                align: 'right'
            });
        buttonText.anchor.set(0.5, 0.5);
        buttonText.position.set(75, 25);
        this.buttonAddCard = new PIXI.Graphics();
        this.buttonAddCard.beginFill(0xf00fff0);
        this.buttonAddCard.drawRect(0, 0, 150, 50);
        this.buttonAddCard.endFill();
        this.buttonAddCard.interactive = true;
        this.buttonAddCard.addChild(buttonText);
        this.buttonAddCard.position.set(700, 400);
        this.buttonAddCard.click = function () {
            config.playButton.container.visible = false;
            var randNumber = config.dealer.getRandomNumber(config.dealer.cardsNumbers.length);
            var card = config.dealer.createCard('' + config.dealer.cardsNumbers[randNumber] + '_of_' + config.dealer.cardsColors[config.dealer.getRandomNumber(config.dealer.cardsColors.length)] + '.png');
            card.x = config.dealer.cardPosition.x + config.dealer.cardXMovement * config.dealer.cards.length;
            card.y = config.dealer.cardPosition.y;
            card.points = config.points[randNumber];
            config.dealer.cards.push(card);
        }.bind(this);
        return this.buttonAddCard;
    }


    HoldButton.prototype.createButtonResults=function () {
        var resultBtn = new PIXI.Text('Final Results',
            {
                fontFamily: 'Arial',
                fontSize: 24,
                fill: 'white',
                align: 'right'
            });
        resultBtn.anchor.set(0.5, 0.5);
        resultBtn.position.set(80, 30);
        var buttonFinalResult = new PIXI.Graphics();
        buttonFinalResult.beginFill(106E16);
        buttonFinalResult.drawRect(0, 0, 160, 60);
        buttonFinalResult.endFill();
        buttonFinalResult.interactive = true;
        buttonFinalResult.addChild(resultBtn);
        buttonFinalResult.position.set(600, 500);
        buttonFinalResult.click=function () {
            config.buttonCard.visible=false;
            this.buttonAddCard.visible=false;
            this.checkResult();
            this.casinoResult()

        }.bind(this);

        return buttonFinalResult;
    }

    HoldButton.prototype.checkResult = function () {
        config.sumOfCasino = 0;
        config.dealer.cards.forEach(function (card) {
            config.sumOfCasino += card.points;
            var playerOneText = new PIXI.Text('Player 1: '+config.sumOfPlayer+".",
                {
                    fontFamily: 'Arial',
                    fontSize: 24,
                    fill: 'white',
                    align: 'right'
                });
            playerOneText.x=50;
            playerOneText.y=300;
        config.stage.addChild(playerOneText);
        })

    HoldButton.prototype.casinoResult=function () {
        var Casino = new PIXI.Text('Casino: '+config.sumOfCasino+".",
            {
                fontFamily: 'Arial',
                fontSize: 24,
                fill: 'white',
                align: 'right'
            });
        Casino.x=400;
        Casino.y=300;
        config.stage.addChild(Casino);
            }

    }
    return HoldButton;
})