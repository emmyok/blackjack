define(['config'],

    function (config) {
        function PlayButton() {
            this.container = new PIXI.Container();
            this.createButton();
            this.checkResult();
        }

        PlayButton.prototype.createButton = function () {
            this.container = new PIXI.Container();
            this.container.addChild(this.createButtonAddCard());
            config.stage.addChild(this.container);
            return this.container;
        }

        PlayButton.prototype.createButtonAddCard = function () {
            var buttonText = new PIXI.Text('Add a Card',
                {
                    fontFamily: 'Arial',
                    fontSize: 24,
                    fill: 'white',
                    align: 'right'
                });
            buttonText.anchor.set(0.5, 0.5);
            buttonText.position.set(75, 25);

           config.buttonCard = new PIXI.Graphics();

            config.buttonCard.beginFill(0xFF2342);
            config.buttonCard.drawRect(0, 0, 150, 50);
            config.buttonCard.endFill();
            config.buttonCard.interactive = true;
            config.buttonCard.addChild(buttonText);
            config.buttonCard.position.set(500, 400);
            config.buttonCard.click = function () {
                var randNumber = config.user.getRandomNumber(config.user.cardsNumbers.length);
                var card = config.user.createCard('' + config.user.cardsNumbers[randNumber] + '_of_' + config.user.cardsColors[config.user.getRandomNumber(config.user.cardsColors.length)] + '.png');
                card.x = config.user.cardPosition.x + config.user.cardXMovement * config.user.cards.length;
                card.y = config.user.cardPosition.y;
                config.user.cards.push(card);
                card.points = config.points[randNumber];
                this.checkResult();

            }.bind(this);
            return  config.buttonCard;
        }

        PlayButton.prototype.checkResult = function () {
            var sum = 0;
            config.user.cards.forEach(function (card) {
                sum += card.points;
            })
            config.sumOfPlayer=sum;
        }



        return PlayButton;
    }
)