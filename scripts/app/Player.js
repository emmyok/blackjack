define(['Card', 'config'], function (Card, config) {

    function Player() {
        this.cardsNumbers = [2, 3, 4, 5, 6, 7, 8, 9, 10, 'ace', 'king', 'jack', 'queen']
        this.cardsColors = ['spades', 'diamonds', 'hearts', 'clubs']
        this.cards = [];
        this.drawCards();
    }

    Player.prototype.createCard = function (fileName1) {
        return new Card(fileName1);
    }


    return Player
})