define(['Player', 'Card', 'config'], function (Player, Card, config) {

    function User() {
        this.cardPosition = {x: 40, y: 200}
        this.cardXMovement = 25
        Player.apply(this, arguments)
    }

    User.prototype = Object.create(Player.prototype);

    User.prototype.drawCards = function () {
        var card;
        var randNumber = this.getRandomNumber(this.cardsNumbers.length);
        card = this.createCard('' + this.cardsNumbers[randNumber] + '_of_' + this.cardsColors[this.getRandomNumber(this.cardsColors.length)] + '.png');
        card.x = this.cardPosition.x;
        card.y = this.cardPosition.y;
        card.points = config.points[randNumber];
        this.cards.push(card);
        console.log(this.cards);
        console.log(card.points, 1);

        var randNumber = this.getRandomNumber(this.cardsNumbers.length);
        card = this.createCard('' + this.cardsNumbers[randNumber] + '_of_' + this.cardsColors[this.getRandomNumber(this.cardsColors.length)] + '.png');
        card.x = this.cardPosition.x + this.cardXMovement;
        card.y = this.cardPosition.y;
        card.points = config.points[randNumber];
        this.cards.push(card);

    }

    User.prototype.getRandomNumber = function (from) {
        var randomNumber = Math.floor(Math.random() * from);
        return randomNumber;
    }
    return User;
})