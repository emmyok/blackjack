define(['pixi'], function (pixi) {

    function Config() {
        this.stage = pixi.getStage();
        this.renderer = pixi.getRenderer();
        this.user;
        this.playButton;
        this.dealer;
        this.points = [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 10, 10, 10, 10];
        this.sumOfPlayer;
        this.sumOfCasino;
        this.buttonCard;
    }

    return new Config();
})

