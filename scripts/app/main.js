define(['pixi', 'User', 'Dealer', 'Engine/Loader', 'Engine/Enum', 'Engine/Events', 'PlayButton', 'config', 'HoldButton'], function (pixi, User, Dealer, Loader, eNum, events, PlayButton, config, HoldButton) {
    function init() {
        var loader = new Loader();
        loader.setAssetsFromFile('./assets/files.json');
        events.on(eNum.EVENTS.ASSETS_FILE_LOADED, loader.load.bind(loader));
        events.on(eNum.EVENTS.ASSETS_LOADED, ready);

        function ready() {
            var user = new User();
            config.user = user;
            var dealer = new Dealer();
            config.dealer = dealer;
            var playButton = new PlayButton();
            config.playButton = playButton;
            new HoldButton();
        }
    }

    init();
});