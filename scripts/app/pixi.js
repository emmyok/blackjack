define(function () {
    function Pixi() {
        this.renderer;
        this.stage;
        this.create();
    }

    Pixi.prototype.gameLoop = function () {
        setTimeout(this.gameLoop.bind(this), 200);
        this.renderer.render(this.stage);
    }

    Pixi.prototype.create = function () {
        this.renderer = PIXI.autoDetectRenderer(1000, 601);
        document.body.appendChild(this.renderer.view);
        this.stage = new PIXI.Container();
        this.gameLoop();
    }

    Pixi.prototype.getStage = function () {
        return this.stage;
    }

    Pixi.prototype.getRenderer = function () {
        return this.renderer;
    }

    return new Pixi();
})